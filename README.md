# [Link to the Wiki Documentation](https://gitlab.com/weftworks/documentation/wikis/home)

## Font Awesome Icon Licence

The icon used in this group and its repositories is provided by Font Awesome and is subject to the CC BY 4.0 License.
* [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)
* [https://fontawesome.com/license](https://fontawesome.com/license)